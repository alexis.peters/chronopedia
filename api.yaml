openapi: 3.0.0

info:
  title: Chronopedia private API
  description: Private API for the website chronopedia.org
  version: 1.0.0

servers:
  - url: https://chronopedia.org/api/v2
    description: Based URL for requesting the API
  - url: /api/v2
    description: Relative URL for development

tags:
  - name: public
    description: Everything about public timelines and events
  - name: connection
    description: Requests for connection
  - name: timeline
    description: Everything about timelines
  - name: event
    description: Everything about events
  - name: user
    description: Everything about users

paths:
  /public/timeline/findByCategory:
    get:
      operationId: timelinesByCategory
      tags:
        - public
      summary: Get a list of timeline with a specified category
      description: Send a category and get a list of timelines'uri in JSON format
      parameters:
        - name: category
          description: The category
          required: true
          in: query
          schema:
            type: string
            example: "Middle-Age"
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelines'
  /public/timeline/{timelineID}/properties:
    get:
      operationId: publicTimelineProperties
      tags:
        - public
      summary: To get the timeline's properties
      description: Ask for a timeline with its uri and get its properties
      parameters:
        - $ref: '#/components/parameters/timelineID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelineProperties'
  /public/timeline/{timelineID}/content:
    get:
      operationId: publicTimelineContent
      tags:
        - public
      summary: To get the timeline's content
      description: Ask for a timeline with uri id and get all the events of the timeline in a JSON array 
      parameters:
        - $ref: '#/components/parameters/timelineID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelineContent'
  /public/event/{eventID}/properties:
    get:
      operationId: publicEventProperties
      tags:
        - public
      summary: To get event properties
      description: Get properties of an event with its uri
      parameters:
        - $ref: '#/components/parameters/eventID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/eventProperties'
  /public/event/{eventID}/content:
    get:
      operationId: publicEventContent
      tags:
        - public
      summary: To get the content of an event
      description: Get the content of an event in a binary file
      parameters:
        - $ref: '#/components/parameters/eventID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/eventContent'

  /login:
    post:
      operationId: login
      tags:
        - connection
      summary: Log into an account
      description: Provide username/email and password and return a session cookie
      requestBody:
        description: credentials (username/email and password)
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                username_or_email:
                  type: string
                  example: "john@company.com"
                password:
                  type: string
      responses:
        '200':
          description: Successfully logged in
          headers:
            Set-Cookie:
              style: simple
              explode: false
              schema:
                type: string
                example: "JSESSIONID=abcde12345; Path=/; HttpOnly"
  /logout:
    post:
      operationId: logout
      tags:
        - connection
      summary: Log out of an account
      description: Send the session cookie to allow the server to delete the session
      security:
        - cookieAuth: []
      responses:
        '200':
          description: Successfully logged out

  /timeline:
    post:
      operationId: createTimeline
      tags:
        - timeline
      summary: To create a timeline
      description: Create an empty timeline and return its uri
      security:
        - cookieAuth: []
      requestBody:
        description: Timeline's properties
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/timelineProperties'
      responses:
        '201':
          description: Timeline created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelineProperties'
  /timeline/findByUser:
    get:
      operationId: timelineByUser
      tags:
        - timeline
      summary: Get a list of timeline of a user
      description: Send the session cookie and get the URIs of the user's timelines in a JSON array
      security:
        - cookieAuth: []
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelines'
  /timeline/{timelineID}/share:
    put:
      operationId: shareTimeline
      tags:
        - timeline
      summary: To share a timeline
      description: Give permissions (editor, commentator or reader) to a group of user. Only the author of the timeline can do this.
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/timelineID'
        - name: message
          description: Specify if you want to notify the users by email
          in: query
          required: false
          schema:
            type: boolean
            default: false
            example: true
      requestBody:
        description: An JSON array of the email adresses of the users you want to share the timeline with
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/userEmail'
      responses:
        '200':
          description: Operation successfull
  /timeline/{timelineID}/properties:
    get:
      operationId: getTimelineProperties
      tags:
        - timeline
      summary: To get the timeline's properties
      description: Ask for a timeline with uri id and get its properties
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/timelineID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelineProperties'
    put:
      operationId: editTimelineProperties
      tags:
        - timeline
      summary: To edit timeline properties (name etc...)
      description: Send properties to edit and their values
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/timelineID'
      requestBody:
        description: object defining timeline properties to edit
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/timelineProperties'
      responses:
        '200':
          description: Operation successfull
  /timeline/{timelineID}:
    get:
      operationId: getTimelineContent
      tags:
        - timeline
      summary: To get the timeline's content
      description: Ask for a timeline with its uri and get all the events of the timeline in a JSON array 
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/timelineID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/timelineContent'
    delete:
      operationId: deleteTimeline
      tags:
        - timeline
      summary: To delete a timeline
      description: Delete the timeline (need to be logged in)
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/timelineID'
      responses:
        '204':
          description: Timeline deleted successfully

  /event:
    post:
      operationId: createEvent
      tags:
        - event
      summary: To create an event
      description: Create an empty event and return its uri
      security:
        - cookieAuth: []
      requestBody:
        description: Event's properties
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/eventProperties'
      responses:
        '201':
          description: Event created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/eventProperties'
  /event/{eventID}/properties:
    get:
      operationId: getEventProperties
      tags:
        - event
      summary: To get event properties
      description: Get properties of an event with its uri
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/eventID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/eventProperties'
    put:
      operationId: editEventProperties
      tags:
        - event
      summary: To edit event properties (title, category etc...)
      description: Send properties to edit and their values
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/eventID'
      requestBody:
        description: object defining event properties to edit
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/eventProperties'
      responses:
        '200':
          description: Operation successfull
  /event/{eventID}/content:
    get:
      operationId: getEventContent
      tags:
        - event
      summary: To get the content of an event
      description: Get the content of an event in a binary file
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/eventID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/eventContent'
    put:
      operationId: editEventContent
      tags:
        - event
      summary: To edit the content of an event
      description: Send the new content to save it
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/eventID'
      requestBody:
        description: The binary file
        content:
          application/octet-stream:
            schema:
              $ref: '#/components/schemas/eventContent'
      responses:
        '200':
          description: Operation successfull
  /event/{eventID}:
    delete:
      operationId: deleteEvent
      tags:
        - event
      summary: To delete an event
      description: Delete the event (need to be logged in)
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/eventID'
      responses:
        '204':
          description: Event deleted successfully
  
  /user:
    post:
      operationId: createUser
      tags:
        - user
      summary: To create a user
      description: Create a user
      requestBody:
        description: User's informations
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                username:
                  $ref: '#/components/schemas/userUsername'
                email:
                  $ref: '#/components/schemas/userEmail'
                password:
                  type: string
      responses:
        '201':
          description: User created successfully
  /user/{userID}/username:
    get:
      operationId: getUserUsername
      tags:
        - user
      summary: To get the username of a user
      description: Get username of a user
      parameters:
        - $ref: '#/components/parameters/userID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                type: object
                properties:
                  username:
                    $ref: '#/components/schemas/userUsername'
  /user/{userID}/email:
    get:
      operationId: getUserEmail
      tags:
        - user
      summary: To get the email of a user
      description: Get email of a user
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/userID'
      responses:
        '200':
          description: Operation successfull
          content:
            application/json:
              schema:
                type: object
                properties:
                  email:
                    $ref: '#/components/schemas/userEmail'
  /user/{userID}:
    put:
      operationId: editUserProperties
      tags:
        - user
      summary: To edit user properties (username, email, password)
      description: Send properties to edit and their values
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/userID'
      requestBody:
        description: object defining user properties to edit
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/userProperties'
      responses:
        '200':
          description: Operation successfull
    delete:
      operationId: deleteUser
      tags:
        - user
      summary: To delete a user
      description: Delete the user (need to be logged in)
      security:
        - cookieAuth: []
      parameters:
        - $ref: '#/components/parameters/userID'
      responses:
        '204':
          description: User deleted successfully

components:
  parameters:
    timelineID:
      name: timelineID
      description: ID of a timeline
      in: path
      required: true
      schema:
        $ref: '#/components/schemas/id'
    eventID:
      name: eventID
      description: ID of an event 
      in: path
      required: true
      schema:
        $ref: '#/components/schemas/id'
    userID:
      name: userID
      description: ID of a user
      in: path
      required: true
      schema:
        $ref: '#/components/schemas/id'
  schemas:
    timelines:
      type: array
      items:
        $ref: '#/components/schemas/timelineURI'
    timelineProperties:
      type: object
      properties:
        title:
          type: string
          example: "My first timeline"
        author:
          type: string
          example: "John"
        is_public:
          type: boolean
          example: true
    timelineContent:
      type: array
      items:
        $ref: '#/components/schemas/eventURI'
    eventProperties:
      type: object
      properties:
        title:
          type: string
          example: "My first event"
        category:
          type: string
          enum:
            - date
            - epoch
          example: date
        start:
          $ref: '#/components/schemas/date'
        end:
          $ref: '#/components/schemas/date'
        weight:
          type: integer
          minimum: 1
          example: 1
        place:
          type: string
          example: "CentraleSupélec"
    eventContent:
      type: string
      format: binary
      x-content-type: application/octet-stream
    userProperties:
      type: object
      properties:
        username:
          $ref: '#/components/schemas/userUsername'
        email:
          $ref: '#/components/schemas/userEmail'
    userUsername:
      type: string
      example: John
    userEmail:
      type: string
      example: john@company.com
    timelineURI:
      type: string
      example: /timeline/102943
    eventURI:
      type: string
      example: /event/102849
    id:
      type: integer
      format: int64
      minimum: 1
      example: 982409
    date:
      type: string
      example: "03/04/2023"
  securitySchemes:
    cookieAuth:
      type: apiKey
      name: JSESSIONID
      in: cookie