# Définition des requêtes

## Navigation sur le site

- Arriver sur le site : espace public
    - Demander des *timelines* publics par catégories
        - Demander les propriétés de chaque *timeline*
        - Demander le contenu de chaque *timeline*
            - Demander les propriétés de chaque *event*
            - Demander le contenu de chaque *event*
    - Espace de connexion :
        - Créer un *user*
        - Demander à se *login*
            - Demander à se *logout*
            - Supprimer le *user*
            - Demander informations *user*
                - Changer *username*
                - Changer *email*
                - Changer *password*
            - Demander *timelines* de son compte
                - Créer une *timeline*
                - Supprimer une *timeline*
                - Demander une *timeline*
                    - Modifier informations de la *timeline*
                    - Créer un *event*
                    - Supprimer un *event*
                    - Demander les propriétés d'un *event*
                        - Modifier propriétés de l'*event*
                    - Demander le contenu d'un *event*
                        - Modifier contenu de l'*event*

## Requêtes nécessaires

- GET : /public/timeline/findByCategory
- GET : /public/timeline/{timelineID}/properties
- GET : /public/timeline/{timelineID}/content
- GET : /public/event/{eventID}/properties
- GET : /public/event/{eventID}/content
- POST : /user
- POST : /login
- POST : /logout
- DELETE : /user/{userid}/delete
- GET : /user/{userid}
- PUT : /user/{userid}
- GET : /timeline/findByUser
- POST : /timeline
- DELETE : /timeline/{timelineid}
- GET : /timeline/{timelineid}/properties
- PUT : /timeline/{timelineid}/properties
- GET : /timeline/{timelineid}/content
- POST : /event
- DELETE : /event/{eventid}
- GET : /event/{eventid}/properties
- PUT : /event/{eventid}/properties
- GET : /event/{eventid}/content
- PUT : /event/{eventid}/content

## Requêtes triées

### public

- GET : /public/timeline/findByCategory
- GET : /public/timeline/{timelineID}/properties
- GET : /public/timeline/{timelineID}/content
- GET : /public/event/{eventID}/properties
- GET : /public/event/{eventID}/content

### connection
- POST : /login
- POST : /logout

### timeline
- GET : /timeline/findByUser
- GET : /timeline/{timelineid}/properties
- PUT : /timeline/{timelineid}/properties
- POST : /timeline
- DELETE : /timeline/{timelineid}

### event
- POST : /event
- GET : /event/{eventid}/properties
- PUT : /event/{eventid}/properties
- GET : /event/{eventid}/content
- PUT : /event/{eventid}/content
- DELETE : /event/{eventid}

### user
- POST : /user
- GET : /user/{userid}
- PUT : /user/{userid}
- DELETE : /user/{userid}

## À vérifier pour l'API

- Il y a bien le security sur les bonnes requêtes (voir cadenas sur la doc)
- Vérifier les required (pas de required pour les requêtes put)
- Les bons objets sont renvoyés (pas d'id, que des uri)
- Les bons codes sont renvoyés par rapport au type de requête