'use strict';

var utils = require('../utils/writer.js');
var Connection = require('../service/ConnectionService');

module.exports.login = function login (req, res, next, body) {
  Connection.login(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.logout = function logout (req, res, next) {
  Connection.logout()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
