'use strict';

var utils = require('../utils/writer.js');
var Event = require('../service/EventService');

module.exports.createEvent = function createEvent (req, res, next, body) {
  Event.createEvent(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteEvent = function deleteEvent (req, res, next, eventID) {
  Event.deleteEvent(eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editEventContent = function editEventContent (req, res, next, body, eventID) {
  Event.editEventContent(body, eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editEventProperties = function editEventProperties (req, res, next, body, eventID) {
  Event.editEventProperties(body, eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getEventContent = function getEventContent (req, res, next, eventID) {
  Event.getEventContent(eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getEventProperties = function getEventProperties (req, res, next, eventID) {
  Event.getEventProperties(eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
