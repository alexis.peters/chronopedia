'use strict';

var utils = require('../utils/writer.js');
var Timeline = require('../service/TimelineService');

module.exports.createTimeline = function createTimeline (req, res, next, body) {
  Timeline.createTimeline(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteTimeline = function deleteTimeline (req, res, next, timelineID) {
  Timeline.deleteTimeline(timelineID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editTimelineProperties = function editTimelineProperties (req, res, next, body, timelineID) {
  Timeline.editTimelineProperties(body, timelineID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getTimelineContent = function getTimelineContent (req, res, next, timelineID) {
  Timeline.getTimelineContent(timelineID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getTimelineProperties = function getTimelineProperties (req, res, next, timelineID) {
  Timeline.getTimelineProperties(timelineID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.shareTimeline = function shareTimeline (req, res, next, body, message, timelineID) {
  Timeline.shareTimeline(body, message, timelineID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.timelineByUser = function timelineByUser (req, res, next) {
  Timeline.timelineByUser()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
