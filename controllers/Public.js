'use strict';

var utils = require('../utils/writer.js');
var Public = require('../service/PublicService');

module.exports.publicEventContent = function publicEventContent (req, res, next, eventID) {
  Public.publicEventContent(eventID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (error) {
      utils.writeError(res, error);
    });
};

module.exports.publicEventProperties = function publicEventProperties (req, res, next, eventID) {
  Public.publicEventProperties(eventID)
    .then(function (response) {
      utils.writeJson(res, response, 200);
    })
    .catch(function (error) {
      utils.writeError(res, error);
    });
};

module.exports.publicTimelineContent = function publicTimelineContent (req, res, next, timelineID) {
  Public.publicTimelineContent(timelineID)
    .then(function (response) {
      utils.writeJson(res, response, 200);
    })
    .catch(function (error) {
      utils.writeError(res, error)
    });
};

module.exports.publicTimelineProperties = function publicTimelineProperties (req, res, next, timelineID) {
  Public.publicTimelineProperties(timelineID)
    .then(function (response) {
      utils.writeJson(res, response, 200);
    })
    .catch(function (error) {
      utils.writeError(res, error)
    });
};

module.exports.timelinesByCategory = function timelinesByCategory (req, res, next, category) {
  Public.timelinesByCategory(category)
    .then(function (response) {
      utils.writeJson(res, response, 200);
    })
    .catch(function (error) {
      utils.writeError(res, error)
    });
};
