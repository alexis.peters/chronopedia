-- Creation of the database

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE IF NOT EXISTS DATABASE `chronopedia` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `chronopedia`;


CREATE IF NOT EXISTS TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE IF NOT EXISTS TABLE `timelines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `is_public` tinyint(3) unsigned DEFAULT NULL,
  `author` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`),
  CONSTRAINT `timelines_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE IF NOT EXISTS TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `category` enum('date','epoch') NOT NULL DEFAULT 'date',
  `start` date NOT NULL,
  `end` date DEFAULT NULL,
  `weight` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `place` varchar(255) DEFAULT NULL,
  `timeline_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timeline_id` (`timeline_id`),
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE IF NOT EXISTS TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE IF NOT EXISTS TABLE `permissions` (
  `user_id` int(10) unsigned NOT NULL,
  `timeline_id` int(10) unsigned NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `timeline_id` (`timeline_id`),
  CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE IF NOT EXISTS TABLE `timelines_has_categories` (
  `timeline_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `timeline_id` (`timeline_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `timelines_has_categories_ibfk_1` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timelines_has_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- Insertion of the data

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
  (1,	"Téo",	"teolefebvre@gmail.com",	"*434580A00F2854F96C02B599BBF514C8CB63739C"),
  (2,	"Alexis",	"alexis.peters.fr@gmail.com",	"*F23DFE5155484909C45EEA7E94DD79BFCD866CDC"),
  (3,	"Arthur",	"arthur.remoue@gmail.com",	"*234FAA6BBCDA359B6D2BDFCCBFF070B5E595021C"),
  (4,	"Matthias",	"mat.orbach@gmail.com",	"*E6419F79B6FB79D70589800C4B2DEE07A6069613");


INSERT INTO `timelines` (`id`, `title`, `is_public`, `author`) VALUES
  (1,	"Première Guerre Mondiale",	1,	1),
  (2,	"Seconde Guerre Mondiale",	1,	1),
  (3,	"Chanson Française",	0,	1);


INSERT INTO `events` (`id`, `title`, `category`, `start`, `end`, `weight`, `place`, `timeline_id`) VALUES
  (1,	"Assassinat de Sarajevo",	"date",	"1914-06-28",	NULL,	1,	"Sarajevo",	1),
  (2,	"Bataille de Verdun",	"epoch",	"1916-02-21",	"1916-12-18",	2,	"Verdun, France",	1),
  (3,	"Bataille du Chemin des Dames",	"epoch",	"1917-04-16",	"1917-10-24",	2,	"Entre Soissons et Reims",	1),
  (4,	"Armistice du 11 novembre",	"date",	"1918-11-11",	NULL,	1,	NULL,	1),
  (5,	"Traité de Versailles",	"date",	"1919-06-28",	NULL,	1,	"Galerie des Glaces, château de Versailles",	1),
  (6,	"Annexion de l\'Autriche",	"date",	"1938-03-11",	NULL,	1,	NULL,	2),
  (7,	"Agression militaire de la Pologne",	"date",	"1939-09-01",	NULL,	1,	NULL,	2),
  (8,	"Entrée en guerre de la France et du Royaume-Uni",	"date",	"1939-09-03",	NULL,	1,	NULL,	2),
  (9,	"Invasion Allemande de l\"URSS",	"date",	"1941-06-22",	NULL,	1,	NULL,	2),
  (10,	"Attaque de Pearl Harbor",	"date",	"1941-12-07",	NULL,	1,	"Pearl Harbor",	2),
  (11,	"Fin de la seconde guerre Mondiale",	"date",	"1945-05-08",	NULL,	1,	NULL,	2),
  (12,	"Création d\"Indochine",	"date",	"1981-01-01",	NULL,	1,	"Paris",	3),
  (13,	"Mistral Gagnant (sortie)",	"date",	"1986-01-01",	NULL,	2,	NULL,	3),
  (14,	"Elle a les yeux revolver (Sortie)",	"date",	"1985-03-05",	NULL,	2,	NULL,	3),
  (15,	"Belle, Notre-Dame de Paris",	"date",	"1998-01-01",	NULL,	2,	NULL,	3),
  (16,	"Il jouait du piano debout",	"date",	"1980-05-19",	NULL,	2,	NULL,	3),
  (17,	"Décès de Claude François",	"date",	"1978-03-11",	NULL,	2,	"Paris",	3);


INSERT INTO `categories` (`id`, `category`) VALUES
(1,	"Histoire"),
(2,	"Musique");


INSERT INTO `permissions` (`user_id`, `timeline_id`, `permission`) VALUES
  (2,	1,	"editor"),
  (3,	1,	"commentator"),
  (4,	1,	"reader");


INSERT INTO `timelines_has_categories` (`timeline_id`, `category_id`) VALUES
  (1,	1),
  (2,	1),
  (3,	2);
