'use strict';


/**
 * To create a user
 * Create a user
 *
 * body User_body User's informations
 * no response value expected for this operation
 **/
exports.createUser = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To delete a user
 * Delete the user (need to be logged in)
 *
 * userID id ID of a user
 * no response value expected for this operation
 **/
exports.deleteUser = function(userID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To edit user properties (username, email, password)
 * Send properties to edit and their values
 *
 * body UserProperties object defining user properties to edit (optional)
 * userID id ID of a user
 * no response value expected for this operation
 **/
exports.editUserProperties = function(body,userID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To get the email of a user
 * Get email of a user
 *
 * userID id ID of a user
 * returns inline_response_200_1
 **/
exports.getUserEmail = function(userID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "email" : "john@company.com"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To get the username of a user
 * Get username of a user
 *
 * userID id ID of a user
 * returns inline_response_200
 **/
exports.getUserUsername = function(userID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "username" : "John"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

