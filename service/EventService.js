'use strict';


/**
 * To create an event
 * Create an empty event and return its uri
 *
 * body EventProperties Event's properties
 * returns eventProperties
 **/
exports.createEvent = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "start" : "03/04/2023",
  "weight" : 1,
  "place" : "CentraleSupélec",
  "title" : "My first event",
  "category" : "date"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To delete an event
 * Delete the event (need to be logged in)
 *
 * eventID id ID of an event
 * no response value expected for this operation
 **/
exports.deleteEvent = function(eventID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To edit the content of an event
 * Send the new content to save it
 *
 * body Object The binary file (optional)
 * eventID id ID of an event
 * no response value expected for this operation
 **/
exports.editEventContent = function(body,eventID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To edit event properties (title, category etc...)
 * Send properties to edit and their values
 *
 * body EventProperties object defining event properties to edit (optional)
 * eventID id ID of an event
 * no response value expected for this operation
 **/
exports.editEventProperties = function(body,eventID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To get the content of an event
 * Get the content of an event in a binary file
 *
 * eventID id ID of an event
 * returns eventContent
 **/
exports.getEventContent = function(eventID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To get event properties
 * Get properties of an event with its uri
 *
 * eventID id ID of an event
 * returns eventProperties
 **/
exports.getEventProperties = function(eventID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "start" : "03/04/2023",
  "weight" : 1,
  "place" : "CentraleSupélec",
  "title" : "My first event",
  "category" : "date"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

