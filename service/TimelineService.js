'use strict';


/**
 * To create a timeline
 * Create an empty timeline and return its uri
 *
 * body TimelineProperties Timeline's properties
 * returns timelineProperties
 **/
exports.createTimeline = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "author" : "John",
  "is_public" : true,
  "title" : "My first timeline"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To delete a timeline
 * Delete the timeline (need to be logged in)
 *
 * timelineID id ID of a timeline
 * no response value expected for this operation
 **/
exports.deleteTimeline = function(timelineID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To edit timeline properties (name etc...)
 * Send properties to edit and their values
 *
 * body TimelineProperties object defining timeline properties to edit (optional)
 * timelineID id ID of a timeline
 * no response value expected for this operation
 **/
exports.editTimelineProperties = function(body,timelineID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * To get the timeline's content
 * Ask for a timeline with its uri and get all the events of the timeline in a JSON array
 *
 * timelineID id ID of a timeline
 * returns timelineContent
 **/
exports.getTimelineContent = function(timelineID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ "/event/102849", "/event/102849" ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To get the timeline's properties
 * Ask for a timeline with uri id and get its properties
 *
 * timelineID id ID of a timeline
 * returns timelineProperties
 **/
exports.getTimelineProperties = function(timelineID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "author" : "John",
  "is_public" : true,
  "title" : "My first timeline"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To share a timeline
 * Give permissions (editor, commentator or reader) to a group of user. Only the author of the timeline can do this.
 *
 * body List An JSON array of the email adresses of the users you want to share the timeline with (optional)
 * message Boolean Specify if you want to notify the users by email (optional)
 * timelineID id ID of a timeline
 * no response value expected for this operation
 **/
exports.shareTimeline = function(body,message,timelineID) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get a list of timeline of a user
 * Send the session cookie and get the URIs of the user's timelines in a JSON array
 *
 * returns timelines
 **/
exports.timelineByUser = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ "/timeline/102943", "/timeline/102943" ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

