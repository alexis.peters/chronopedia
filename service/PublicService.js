'use strict';

const Database = require("../class/db")

/**
 * To get the content of an event
 * Get the content of an event in a binary file
 *
 * eventID id ID of an event
 * returns eventContent
 **/
exports.publicEventContent = function(eventID) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * To get event properties
 * Get properties of an event with its uri
 *
 * eventID id ID of an event
 * returns eventProperties
 **/
exports.publicEventProperties = function(eventID) {
  return new Promise(function(resolve, reject) {
    const db = new Database()
    db.getEvPropByEvId(eventID)
    .then(evProp => {
      resolve(evProp)
    })
    .catch(error => {
      reject(error)
    })
    db.end()
  });
}


/**
 * To get the timeline's content
 * Ask for a timeline with uri id and get all the events of the timeline in a JSON array
 *
 * timelineID id ID of a timeline
 * returns timelineContent
 **/
exports.publicTimelineContent = function(timelineID) {
  return new Promise(function(resolve, reject) {
    const db = new Database()
    db.getTlContByTlId(timelineID)
    .then(tlCont => {
      resolve(tlCont)
    })
    .catch(error => {
      reject(error)
    })
    db.end()
  });
}


/**
 * To get the timeline's properties
 * Ask for a timeline with its uri and get its properties
 *
 * timelineID id ID of a timeline
 * returns timelineProperties
 **/
exports.publicTimelineProperties = function(timelineID) {
  return new Promise(function(resolve, reject) {
    const db = new Database()
    db.getTlPropByTlId(timelineID)
    .then(tlProp => {
      resolve(tlProp)
    })
    .catch(error => {
      reject(error)
    })
    db.end()
  })
}


/**
 * Get a list of timeline with a specified category
 * Send a category and get a list of timelines'uri in JSON format
 *
 * category String The category
 * returns timelines
 **/
exports.timelinesByCategory = function(category) {
  return new Promise(function(resolve, reject) {
    const db = new Database()
    db.getCatIdByCatName(category)
    .then(catId => {
      return db.getTlIdsByCatId(catId)
    })
    .then(ids => {
      let results = []
      ids.forEach(id =>  {
        results.push('/public/timeline/' + String(id))
      })
      resolve(results)
    })
    .catch(error => {
      reject(error)
    })
    db.end()
  })
}
