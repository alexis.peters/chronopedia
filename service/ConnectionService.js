'use strict';


/**
 * Log into an account
 * Provide username/email and password and return a session cookie
 *
 * body Login_body credentials (username/email and password)
 * no response value expected for this operation
 **/
exports.login = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Log out of an account
 * Send the session cookie to allow the server to delete the session
 *
 * no response value expected for this operation
 **/
exports.logout = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

