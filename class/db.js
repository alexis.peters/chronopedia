const mysql = require("mysql")
const dotenv = require("dotenv").config().parsed

class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host: dotenv.DB_HOST,
            user: dotenv.DB_USER,
            password: dotenv.DB_USER_PASSWORD,
            database: dotenv.DB_NAME
        })
        this.connection.connect()
    }

    checkResult(results) {
        return results === undefined || results.length === 0
    }

    getCatIdByCatName(category) {
        let query = "SELCT categories.id as id FROM categories WHERE category LIKE ?"
        return new Promise((resolve, reject) => {
            this.connection.query(query, [category], (error, results, fields) => {
                if (error) reject(error)
                if (this.checkResult(results)) reject(Error("ResourceNotFound"))
                else resolve(results[0]["id"])
            })
        })
    }

    getTlIdsByCatId(catId) {
        return new Promise((resolve, reject) => {
            let query = "SELECT timeline_id FROM timelines_has_categories WHERE category_id = ?"
            this.connection.query(query, [catId], (error, results, fields) => {
                if (error) reject(error)
                if (this.checkResult(results)) {
                    reject(Error("ResourceNotFound"))
                } else {
                    let ids = []
                    results.forEach(row => {
                        ids.push(row.timeline_id)
                    })
                    resolve(ids)
                }
            })
        })
    }

    getTlPropByTlId(tlId) {
        return new Promise((resolve, reject) => {
            let query = "SELECT title, is_public, author FROM timelines WHERE id = ?"
            this.connection.query(query, [tlId], (error, results, fields) => {
                if (error) reject(error)
                if (this.checkResult(results)) reject(Error("ResourceNotFound"))
                else resolve(results[0])
            })
        })
    }

    getTlContByTlId(tlId) {
        return new Promise((resolve, reject) => {
            let query = "SELECT events.id AS id FROM events JOIN timelines ON events.timeline_id = timelines.id WHERE timelines.id = ?"
            this.connection.query(query, [tlId], (error, results, fields) => {
                if (error) reject(error)
                if (this.checkResult(results)) reject(Error("ResourceNotFound"))
                let content = []
                results.forEach(row => {
                    content.push("/public/event/" + row.id)
                })
                resolve(content)
            })
        })
    }

    getEvPropByEvId(evId) {
        return new Promise((resolve, reject) => {
            let query = "SELECT title, category, start, end, weight, place, timeline_id FROM events WHERE id = ?"
            this.connection.query(query, [evId], (error, results, fields) => {
                if (error) reject(error)
                if (this.checkResult(results)) reject(Error("ResourceNotFound"))
                resolve(results[0])
            })
        })
    }

    end() {
        this.connection.end()
    }
}

module.exports = Database
// const db = new Database()
// db.getCatIdByCatName("test").then(catId => {
//     return db.getTlIdsByCatId(catId)
// }).then(ids => {
//     console.log(ids)
// }).catch(error => {
//     console.log("error catch")
//     throw error
// })

// db.getTlPropByTlId(3).then(props => {
//     console.log(props)
//     db.end()
// }).catch(error => {
//     throw error
// })

// db.getTlContByTlId(3).then(content => {
//     console.log(content)
//     db.end()
// }).catch(error => {
//     throw error
// })

// db.getEvPropByEvId(2).then(props => {
//     console.log(props)
// })